package com.example.tugasankolayout

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        ViewLogin().setContentView(this)
        super.onCreate(savedInstanceState)
        val nama = findViewById<EditText>(R.id.nama)
        val btn = findViewById<Button>(R.id.btn)

        btn.setOnClickListener {
            val namas = nama.text.toString().trim()
            startActivity(intentFor<Home>("nama" to namas))
            finish()
        }
    }

    class ViewLogin : AnkoComponent<MainActivity> {
        override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                    padding = 30
                    verticalGravity = Gravity.CENTER
                    backgroundColor = R.color.abu
                }
                verticalLayout {
                    lparams(width = matchParent, height = wrapContent) {
                        backgroundColor = Color.WHITE
                        margin = 15
                        padding = 20
                    }
                    textView {
                        text = "Login"
                        textSize = 20.0f
                        textColor = Color.CYAN
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                    editText {
                        hint = "Input Your Name"
                        id = R.id.nama
                        inputType = InputType.TYPE_CLASS_TEXT
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15


                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                    button("Login") {
                        id = R.id.btn
                    }.lparams(width = wrapContent, height = wrapContent) {
                        margin = 15
                        gravity = Gravity.CENTER
                    }

                }
            }
        }

    }
}
