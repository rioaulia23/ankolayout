package com.example.tugasankolayout

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class Home : AppCompatActivity(), AnkoLogger {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewHome().setContentView(this)
        val text: TextView = findViewById(R.id.text)
        text.text = intent.getStringExtra("nama")


    }

    override fun onBackPressed() {
        alert(title = "Warning", message = "password atau username salah") {
            positiveButton(buttonText = "ok") {
                finish()
            }
            negativeButton(buttonText = "tidak") {
                it.dismiss()
            }
        }.show()
    }

    class ViewHome : AnkoComponent<Home> {
        override fun createView(ui: AnkoContext<Home>) = with(ui) {
            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                    padding = 30
                    backgroundColor = R.color.abu
                }
                verticalLayout {
                    lparams(width = 850, height = 100) {
                        textView {
                            text = "tes"
                            textSize = 20.0f
                            textColor = Color.YELLOW
                            id = R.id.text
                        }
                    }
                }

                linearLayout {
                    onClick {
                        toast("Tes 1")
                    }
                    lparams(width = 850, height = 300) {
                        backgroundColor = Color.WHITE
                        margin = 50
                        padding = 20

                    }
                    imageView {
                        setImageResource(R.drawable.rich)
                    }.lparams(300, 300)
                    textView {
                        text = "tes"
                        textSize = 20.0f
                        textColor = Color.BLACK
                        id = R.id.tes1
                    }

                }
                linearLayout {
                    onClick {
                        toast("Tes 2")
                    }
                    lparams(width = 850, height = 300) {
                        backgroundColor = Color.WHITE
                        margin = 50
                        padding = 20

                    }
                    imageView {
                        setImageResource(R.drawable.rich)
                    }.lparams(300, 300)
                    textView {
                        text = "tes"
                        textSize = 20.0f
                        textColor = Color.BLACK
                        id = R.id.tes2
                    }
                }
                linearLayout {
                    onClick {
                        toast("Tes 3")
                    }
                    lparams(width = 850, height = 300) {
                        backgroundColor = Color.WHITE
                        margin = 50
                        padding = 20

                    }
                    imageView {
                        setImageResource(R.drawable.rich)

                    }.lparams(300, 300)
                    textView {
                        text = "tes"
                        textSize = 20.0f
                        textColor = Color.BLACK
                        id = R.id.tes3
                    }
                }
            }
        }
    }
}
